import { createRouter,createWebHashHistory } from 'vue-router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

const routerHashHistory = createWebHashHistory();

const router = createRouter({
	history:routerHashHistory,
	routes: [
		{
			path: '/',
			redirect:'/login'
		},
		{
			path: '/login',
			name: 'loginIndex',
			component: () => import('/@/views/login/index.vue'),
			meta: {
				keepAlive: true,
				title:'登录'
			}
		},
		{
			path: '/home',
			name: 'homeIndex',
			component: () => import('/@/views/home/index.vue'),
			meta: {
				keepAlive: false,
				title:'首页'
			}
		}
	],
});

// 路由加载前
router.beforeEach(async (to, from, next) => {
	NProgress.configure({ showSpinner: false });
	if (to.meta.title) NProgress.start();
	next();
});

// 路由加载后
router.afterEach(() => {
	NProgress.done();
});

// 导出路由
export default router;